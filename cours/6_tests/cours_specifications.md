---
title: "Chapitre 6: Spécification et tests des programmes"
author: B. Lenoir
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
book: True
fontfamily: lmodern
documentclass: book
geometry:
- hmargin=10mm
- vmargin=20mm
- heightrounded
colorlinks: true
listings-disable-line-numbers: true
caption-justification: centering
float-placement-figure: h

header-includes:
- |
  ```{=latex}
  \usepackage{awesomebox}
  \usepackage{bclogo}
  ```
pandoc-latex-environment:
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
---

<!-- pandoc --template=eisvogel_190520.tex --highlight-style tango --filter pandoc-latex-environment -s cours_specifications.md -o cours_specifications.pdf -->

# \centering Spécification et tests de programmes

---

::: note
La __spécification__  d'une fonction consiste à décrire de manière explicite ce que doit faire cette fonction. 
:::

Nous allons voir comment spécifier et tester son programme pour être le plus rigoureux possible.

## __I. Spécification des programmes__

### __1. Documenter son programme__

Il est fortement conseillé de donner une _documentation_ (description) à chaque fonction c'est ce que nous avons déjà fait à l'aide de __docstrings__

```python
def division_euclidienne(a, b):
    """ 
    Renvoie le quotient et le reste de la division euclidienne de a par b
    """
    quotient = a // b  
    reste = a % b  
    return [quotient, reste] 
```

L'utilisateur peut ainsi comprendre à quoi sert chacune des fonctions :

```python
>>> help(division_euclidienne)

 Help on function division_euclidienne in module __main__:

 division_euclidienne(a, b)
     renvoie le quotient et le reste de la division euclidienne de a par b
```


La documentation pour être réellement efficace doit cependant contenir plus d’informations : ici par exemple l’utilisation de cette fonction sous-entend que `a` et `b` sont des entiers.

Il est possible d’améliore la docstring de cette manière :

```python
def division_euclidienne(a, b):
    """
    Renvoie le quotient et le reste d'une division euclidienne

    :param a: diviseur
    :type a: int
    :param b: dividende
    :type b : int
    :return: [quotient, reste]
    :rtype: list[int]
    """

    # suite de la fonction
```

::: note
Les informations peuvent être structurées en utilisant un format appelé __ReStructuredText (reST)__.

$\bullet$ chaque paramètre associé est décrit ainsi:\newline
`:param <nom_du_parametre>: <description>`

$\bullet$ le type de données est aussi explicité pour chacun d’eux:\newline
`:type <nom_du_parametre>: <type de donnees>`

$\bullet$ la valeur de retour est aussi décrite:\newline
`:return: <description>`

$\bullet$ ainsi que son type:\newline
`:rtype:`
:::

Il est aussi possible de synthétiser le type et le nom des paramètres en une seule ligne:
```python
def division_euclidienne(a, b):
    """
    Renvoie le quotient et le reste d'une division euclidienne
    
    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste]
    :rtype: list[int]
    """

    # suite de la fonction
```

::: note
La syntaxe compacte pour décrire un parametre et son type est:\newline
`:param <type> <nom>: <description>`
:::


### __2. Préconditions__

::: note
Les __préconditions__ doivent garantir que l’exécution du traitement est possible sans erreur.
:::

Toujours en reprenant le même exemple que précédemment, le fait de décrire que `a` et `b` soient entiers ne suffit pas à garantir le bon fonctionnement. 
On peut lister plusieurs préconditions:

* `a` et `b` sont deux entiers
* `a`$\geq$`0` et `b>0`

::: note
Il est possible de faire apparaitre ces préconditions dans les docstrings à l’aide d’une rubrique appelé _Contraintes d’utilisation_ `:CU:` pour spécifier encore davantage la fonction.
:::

Par exemple :
```python
def division_euclidienne(a, b):
    """
    Renvoie le quotient et le reste d'une division euclidienne
    
    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste]
    :rtype: list[int]
    
    :CU: a >= 0 and b > 0
    """

    # suite de la fonction
```

\newpage

## __II. Programmation défensive__


::: caution
La documentation ne protége pas d'une mauvaise utilisation d'une fonction surtout si on ne la lit pas!
:::

Si on souhaite éviter des erreurs ou des résultats incohérents on peut interrompre le programme dès lors que la fonction reçoit une information non valide ou renvoyer des valeurs spéciales: c'est ce qu'on appelle la __programmation défensive__

### __1. Assertions__

::: note
Une __assertion__ est une expression qui doit être évaluée à vrai. Si cette évaluation échoue elle peut mettre fin à l'exécution du programme. L'utilisation du mot clé `assert` en Python permet de réaliser des tests unitaires.\newline
Elle a pour syntaxe : `assert condition , "message"`
:::

$\bullet$ Voici un premier exemple:
```python
>>> a=-3
>>> assert a > 0, "a doit être positif"
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
AssertionError: a doit être positif
```

Ici l’assertion `a > 0` n’est pas vraie, le programme s’interrompt et renvoie le message d’erreur associé que nous avons préalablement choisi `"a doit être positif"`.

$\bullet$ Dans le cas de la fonction `division_euclidienne` on pourrait par exemple s'assurer que le dividende est entier mais aussi strictement positif avant de calculer quotient et reste.\
En fait on va vérifier une ou plusieurs préconditions avant l’exécution du code de la fonction:

```python
def division_euclidienne(a, b):
    """
    Renvoie le quotient et le reste d'une division euclidienne
    
    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste]
    :rtype: list[int]
    
    :CU: a >= 0 and b > 0
    """

    assert type(b) == int , "le dividende doit être entier"
    assert b != 0 , "le dividende ne peut être nul"
    
    quotient = a // b
    reste = a % b
    return [quotient, reste]
```

Réalisons maintenant un appel ne vérifiant pas la première précondition :

```python
>>> division_euclidienne(5, 1.5)
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "C:\Travail\2020-2021\1ere_spe_nsi\6_tests\tests.py", line 13, in division_euclidienne
    assert type(b) == int , "le dividende doit être entier"
AssertionError: le dividende doit être entier
```

La première assertion n’est pas vraie: le programme est interrompu et un message d’erreur permet
d’expliciter ce qui pose problème.

::: note
L’utilisation du mot clé `assert` en Python permet de réaliser des tests unitaires.
:::


### __2. La valeur spéciale `None`__

Une autre façon d’être défensif consiste, plutôt que d’échouer, à renvoyer une valeur qui ne peut être confondue avec un résultat valide.

```python
def division_euclidienne(a, b):
    # docstring
    
    if b == 0:
        return None
    
    # suite des instructions

>>> division_euclidienne(5, 0)
>>>
```

::: note
La valeur spéciale `None` est utilisée pour représenter l’absence de valeur.\newline
C’est la même qui est renvoyée par les procédures (comme les affichages): son type est `NoneType`
:::

```python
>>> type(print())
<class 'NoneType'>
```


## __III. Vérifier la validité de vos programmes__

::: caution
Spécifier sa fonction et vérifier les préconditions peuvent ne pas suffir à garantir la validité de votre programme. En effet ce n’est pas parce que votre programme vous renvoie une valeur que c’est la bonne!
:::

### __1. Postconditions__

::: note
Les postconditions doivent garantir que le traitement a bien réalisé son travail.
:::

Toujours dans le cas de la fonction `division_euclidienne`, une postcondition est par exemple:
$$ a = quotient \times b + reste$$

On pourrait tout à fait vérifier à l’aide d’assertions les postconditions dans une fonctions mais attention à ne pas surcharger votre code!

::: tip
Les invariants de boucle sont des exemples de postconditions pour valider la correction d’une boucle.
:::


### __2. Le module `doctest`__

#### __Présentation__

Le module `doctest` permet d’inclure les tests dans la docstring descriptive de la fonction écrite. On présente dans la docstring, en plus des explications d’usage, des exemples d’utilisation de la fonction tels qu’ils pourraient être tapés directement dans le Shell.

```python
def division_euclidienne(a, b):
    """
    Renvoie le quotient et le reste d'une division euclidienne
    
    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste]
    :rtype: list[int]
    
    :CU: a >= 0 and b > 0
    
    :examples:
    >>> division_euclidienne(5, 2)
    [2, 1]
    >>> division_euclidienne(11, 3)
    [3, 2]
    """

    # suite des instructions
```

::: note
Les doctests doivent être placés dans la rubrique `:examples:` (_exemple_ en anglais) de la docstring.
:::

Afin d’exécuter l’ensemble des tests on peut utiliser dans le Shell les instructions suivantes :

```python
>>> import doctest
>>> doctest.testmod()

TestResults(failed=0, attempted=2)
```

La fonction `testmod` du module `doctest` est allée chercher dans les docstring des fonctions du module actuellement chargé tous les exemples (reconnaissables à la présence des triples chevrons `>>>`), et a vérifié que la fonction documentée satisfait bien ces exemples. 

Dans le cas présent, une seule fonction dont la documentation contient deux exemples (`attempted=2`) a été testée, et il n’y a eu aucun échec (`failed=0`).


Si les résultats annoncés dans le doctest ne correspondent pas à ceux qui sont renvoyés par la console, chacun des tests est détaillé.

Faussons volontairement le premier test le résultat du premier exemple dans la docstring puis relançons la vérification:

```python
    '''
    :examples:
    >>> division_euclidienne(5, 2)
    [4, 1]
	'''
```

```python
**********************************************************************
File "tests.py", line 13, in __main__.division_euclidienne
Failed example:
    division_euclidienne(5, 2)
Expected:
    [4, 1]
Got:
    [2, 1]
**********************************************************************
1 items had failures:
   1 of   2 in __main__.division_euclidienne
***Test Failed*** 1 failures.
```

#### __Automatiser les doctests dans votre script__

Le nombre d’espace dans les tests a une influence puisque le module `doctest` vérifie la syntaxe exacte fournie par la docstring. Afin d’éviter ce problème et de lancer votre jeu de tests dès l’exécution du script, ajoutez les lignes suivantes à la fin du script:

```python
if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
```

::: tip
Vous remarquerez également qu’aucun message n’est affiché si tous les tests passent avec succés (`verbose = False`).
:::

## __IV. Bon ensemble de tests__

Même si l’écriture de tests ne garantit en rien qu’un programme est correct, le fait de réfléchir aux cas limites que l’on peut rencontrer et en écrire une vérification explicite permet de délimiter correctement le problème.

Il est difficile de déterminer si l'ensemble de tests est suffisant mais on peut déjà se baser sur les points suivants :

* Tester les __cas particuliers__ évoqués dans la description des paramètres.    
  
* Si une fonction renvoie un __booléen__, __prévoir au moins deux tests__ avec des résultats différents.    

* Si une fonction utilise un __nombre__, prévoir des tests pour des __valeurs positives, négatives et nulle__.    

* Si une fonction fait intervenir des valeurs appartenant à un intervalle, il est utile de prévoir des __tests aux limites de cet intervalle__.  
