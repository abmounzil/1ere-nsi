---
title: "TD: Spécifications et tests de programmes"
author: M.A.M
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
book: True
fontfamily: lmodern
documentclass: article
geometry:
- hmargin=10mm
- vmargin=20mm
- heightrounded
colorlinks: true
listings-disable-line-numbers: true
caption-justification: centering
float-placement-figure: h

header-includes:
- |
  ```{=latex}
  \usepackage{awesomebox}
  \usepackage{bclogo}
  ```
pandoc-latex-environment:
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
---


<!-- pandoc --template=eisvogel_190520.tex --highlight-style tango --filter pandoc-latex-environment -s td_specifications_tests.md -o td_specifications_tests.pdf -->


# \hspace{5cm} TD: Spécifications et tests de programmes

---

## __Exercice 1: Documentation au format reST__

__1.__ On considère la fonction suivante renvoyant la somme de dés 6 :

```python
def total_des(de1, de2):
    return de1 + de2
```

Ecrire une docstring complète dans laquelle vous rajouterez une contrainte d'utilisation.

__2.__ On considère la procédure suivante affichant le nom, le prénom et la taille en mètres d'une personne:

```python
def affichage_infos(nom, prenom, taille):
    print("Mes informations personnelles :", nom, prenom, str(taille)+"m")
```

Ecrire une docstring complète.


## __Exercice 2: Préconditions et postconditions__

__1.__ On considère la fonction racine carrée appliquée sur un entier.

Trouver une précondition et deux postconditions (_plus difficile_) sur son utilisation. _On ne s'interessera pas au type de données._

__2.__ Soit la fonction suivante renvoyant le solde d'un compte bancaire qu'on souhaite toujours créditeur (ou à zéro)

```python
def nouveau_solde(retrait, solde):
    solde = solde - retrait
    return solde
```
__a.__ Trouver une précondition et une postcondition.

__b.__ Ecrire la docstring correspondante en ajoutant des contraintes d'utilisations


## __Exercice 3: Assertions__

__1.__ On considère la fonction suivante `mention` renvoyant la mention obtenue au BAC pour une moyenne passée en paramètre:

```python
def mention(moyenne):
    if moyenne < 12:
        return None
    elif moyenne < 14:
        return 'AB'
    elif moyenne <16:
        return 'B'
    else :
        return 'TB'
```

__a.__ Ecrire une assertion vérifiant le type du paramètre `moyenne` puis rédiger la docstring correspondante.

__b.__ Ecrire deux assertions permettant d'éviter des moyennes incohérentes.

::: tip
Lorsque plusieurs types de données sont possibles pour un paramètre, on peut utiliser la syntaxe suivante :

```python
"""
:param a: description du paramètre a
:type a: bool or int
"""
```

Ici par exemple le paramètre `a` peut être un booléen ou un entier.
:::

__2.__ On considère la fonction suivante renvoyant la valeur maximale présente dans un tableau d'entiers passé en paramètre:

```python
def max_tableau(tab):
    max_value = tab[0]
    for nombre in tab :
        if nombre > max_value:
            max_value = nombre
    return max_value
```

__a.__ Quelle est la précondition sur le tableau passé en paramètre? _(Pensez au cas limite)_

__b.__ Ecrire l'assertion correspondante.  

__c.__ Donner une deuxième version `max_tableau_bis` de cette fonction sans utiliser d'assertion mais en utilisant la valeur `None` comme valeur de retour.  


## __Exercice 4: Doctests__
   
__1. Fonction puissance__

__a.__ Donner une chaîne de documentation (pour l'instant sans exemples) pour la fonction suivante qui calcule `x` à la puissance `n` pour deux entiers `x` et `n`.

```python
def puissance(x, n):
    return x ** n
```

__b.__ Etablir un jeu de quelques tests et charger le module doctest au lancement du script comme vu en cours.

__c.__ Modifier un des tests afin qu'il échoue et observer le résultat.


__2. Fonction `max_tableau`__

__a.__ Reprendre la fonction `max_tableau` de l'exercice précédent et établir une docstring compléte avec un jeu de tests que vous appliquerez.

__b.__ Les doctests peuvent aussi s'appliquer aux exceptions levées lors d'une erreur comme `AssertionError`.  
Voici un exemple d'utilisation dans la docstring: 

```python
"""
>>> max_tableau([])
Traceback (most recent call last):
    ...
AssertionError: le tableau ne peut être vide

"""
```
__Essayer d'intégrer cet exemple à vos doctests en adaptant le message d'erreur au votre.__


## __Exercice 5:  Bon jeu de tests__

_(Extrait de Numérique et Sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen))_

On prétend que le prédicat `appartient`, défini juste après, teste l'appartenance de la valeur `v` au tableau `t`.

```python
def appartient(v,t):
    i = 0
    while i < len(t)- 1 and t[i] !=v:
        i = i + 1
    return i < len(t)        
```

Par exemple : 

```python
>>> appartient(4, [1,3,4,8])

 True
```
  
__1.__ Etablir un jeu de quelques tests (dans certains cas `v `est présent dans le tableau, dans d'autres non) puis utiliser les avec le module doctest.

_Vous devez constater des erreurs car la fonction ne réalise en fait pas ce qu'on attend d'elle._   
__2.__ Expliquer ce que fait en réalité la fonction `appartient`.
