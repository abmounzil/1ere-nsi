# 1ère NSI

Projet regroupant l'ensemble des cours, TD et TP dispensés en 1ère NSI 2020/2021

* La plupart des documents proposés sur ce site sont sous licence creative commons : BY-NC-SA.

* Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre.
Merci de respecter les conditions de la licence

Pour tous les autres : les sources sont citées

---
# Séquence 9: Les tuples

* [Cours: Les tuples](./cours/9_tuples/cours_tuples.pdf)

* [TD/TP: Les tuples](./cours/9_tuples/tp_tuples.pdf)

---
# Séquence 8: IHM (partie 2)

À venir

---
# Séquence 7: Les tris

À venir

---
# Séquence 6: Les spécifications

* [Cours: Spécifications et tests de programme](./cours/6_tests/cours_specifications.pdf)

* [TD: Quelques applications](./cours/6_tests/td_specifications_tests.pdf)





--- 
# Mini-Projet: 

---
